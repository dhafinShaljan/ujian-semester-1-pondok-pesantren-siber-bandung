<!DOCTYPE html>
<html>
<head>
	<title>Detik.co.id</title>
	<link rel="stylesheet" href="css/login.css"/>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>

<div class="login-box">
	<h1>Login</h1>
	<form action="index.php" method="post">
	<div class="textbox">
		<i class="fa fa-user" aria-hidden="true"></i>
		<input type="text" placeholder="Username" name="username">
	</div>
	<div class="textbox">
		<i class="fa fa-lock" aria-hidden="true"></i>
		<input type="Password" placeholder="Password" name="password">
	</div>

<input class="btn" type="submit" name="Login" value="Sign in">
</form>
</div>
</body>
</html>