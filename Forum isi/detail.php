<?php  
// session_start();

// if( !isset($_SESSION["login"]) ) {
// 	header("Location: login.php");
// 	exit;
// }

require 'fungsi/fungsi_user.php';
require 'fungsi/config.php';

$rowForum = detailForum($_GET['id']);
$rowKomentar = tampilKomentar($_GET['id']);

?>


<!DOCTYPE html>
<html>
<head>
	<title>Forum Diskusi</title>
	<link rel="stylesheet" type="text/css" href="assets/css/detail.css">
</head>
<body>

	

	<div class="container">
		<div>
			<div class="forum-bahasan">
				<h2><?= $rowForum['judul'] ?></h2>
				<p>
				<?= $rowForum['isi'] ?>
				</p>
			</div>
			<div class="input-comment">
				<form action="" method="post">
					<fieldset>
						<h3>Write a comment</h3>
						<div>
							<label for="nama">Nama</label>
							<input type="text" name="nama" id="nama" required="">
						</div>
						<div>
							<label for="editor">Isi Komentar</label>
							<textarea name="isi" id="editor" rows="10"></textarea>
						</div>
						<button type="submit" name="btnkomen">
							Masukkan Komentar
						</button>
					</fieldset>
				</form>
			</div>


				<?php  
					if ( isset($_POST['btnkomen']) ) {
						postKomentar($_POST, $_GET['id']);

						echo "<meta http-equiv='refresh' content='1.5;url=detail.php?id=".$rowForum['id']."'>";
					}

				?>

				<!-- <div>Alert</div> -->
			<div id="komentar">
				<hr>
				<h3>Komentar :</h3>
			</div>

			<div>
				<?php foreach ($rowKomentar as $row) : ?>

					<div class="comments">
						<h4><?= $row['nama'] ?></h4>
						<p><?= $row['isi'] ?></p>
					</div>

				<?php endforeach ?>
			</div>	

			<div>
				<br>
				<a href="index.php" style="text-decoration: none; background-color: #d12e2e; color: white; padding: 7px 15px; border-radius: 7px; margin-top: 10px; border: none; font-family: inherit;">
					Kembali
				</a>
			</div>
		</div>
	</div>
	

	<script src="assets/js/ckeditor.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>