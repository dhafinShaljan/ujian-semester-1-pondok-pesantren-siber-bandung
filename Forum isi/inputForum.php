<?php  
// session_start();

// if( !isset($_SESSION["login"]) ) {
// 	header("Location: login.php");
// 	exit;
// }

require 'fungsi/fungsi_user.php';
require 'fungsi/config.php';

?>


<!DOCTYPE html>
<html>
<head>
	<title>Forum Diskusi</title>
	<link rel="stylesheet" type="text/css" href="assets/css/inputForum.css">
</head>
<body>
	<style type="text/css">
		body 
		{
			background-image:url(assets/img/forum.jpg);
			background-size:1200px;
			color: #d12f2f;
		}
	</style>
	


	

	<div class="container">
		<div class="input-forum">
			<form action="" method="post">
				<fieldset>
					<h3>Input Forum</h3>
					<div>
						<label for="judul">Judul</label>
						<input type="text" name="judul" id="judul" required="">
					</div>
					<div>
						<label for="editor">Isi</label>
						<textarea name="isi" id="editor"></textarea>
					</div>
					<button type="submit" name="btnsimpan">
						Simpan
					</button>
				</fieldset>
			</form>
		</div>

		<?php if( isset($_POST['btnsimpan']) ) {
					postForum($_POST);
					echo "<meta http-equiv='refresh' content='1;url=index.php'>";
				}
		?>
		<hr>
		<a href="index.php">
			<button style="background-color: #d12e2e; color: white; padding: 7px 15px; border-radius: 7px; margin-top: 10px; border: none; font-family: inherit;">
				kembali
			</button>
		</a>
	</div>


	<script src="assets/js/ckeditor.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>