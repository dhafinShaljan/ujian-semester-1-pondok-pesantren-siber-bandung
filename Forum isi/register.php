<?php 

	require 'fungsi/fungsi_user.php';

	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	}

	if (isset($_POST["register"]) ) {

		if (registrasi($_POST) > 0) {
			echo "<script>
					alert('user baru berhasil ditambahkan!');
					</script>";
		} else {
			echo mysqli_error($conn);
		}
	}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Registrasi</title>
	<style type="text/css">

		body{
			background-image: url(assets/img/register.jpg);
			background-size: 1250px;
			color:white;
			
		}

		h1{
			font-family: "montserratff", helvetica, arial;
		}

		fieldset{
			width: 350px;
		}

		ul li{
			list-style: none;
			margin-top: 5px;
			margin-right: 32.5px;
			padding: 5px;
		}

		input{
			outline: none;
			padding: 5px 10px;
			background-color: #e8e8e8;
			font-family: Helvetica;
			border: solid 3px #e8e8e8;
			border-radius: 5px;
			transition: all 0.5s;
		}

		input:focus{
			background-color: white;
			border: solid 3px #0B8389;
			transition: all 0.5s;
		}


		label{
			display: block;
			font-family: Helvetica;
		}


		button {
			padding: 10px 20px;
			color: white; 
			border-radius: 10px;
			border: none;
			margin-top: 15px;
			font-weight: bold;

		}

		button[type="submit"]{
			background-color: #23b2c2;
			border-bottom: solid 2px #036a6f;
		}

		button[type="reset"]{
			background-color: #d12f2f;
			border-bottom: solid 2px #af1f1f;
		}

		button:hover{
			opacity: 0.8;
		}

		@keyframes fadeIn{

			from {
				opacity: 0;
			}

			to {
				opacity: 1;
			}
		}
	</style>
</head>
<body>
	<center>
	<h1>Halaman Registrasi</h1>

	<form action="" method="post">
		<fieldset width="500">
		<ul>
			<li>
				<label for="username">Username :</label>
				<input type="text" name="username" id="username" required="">
			</li>
			<li>
				<label for="password">Password :</label>
				<input type="password" name="password" id="password" required="">
			</li>
			<li>
				<label for="password2">Konfirmasi Password :</label>
				<input type="password" name="password2" id="password2" required="">
			</li>
			<li>
				<label for="email">Email :</label>
				<input type="text" name="email" id="email">
			</li>
			<li>
				<label for="asal">Asal :</label>
				<input type="text" name="asal" id="asal">
			</li>
			<li>
				<button type="submit" name="register">Daftar</button>
				<button type="reset">Reset</button>
			</li>
		</ul>
		</fieldset>
	</form>
	<h4 style="margin-bottom: 0px;">Jika sudah mendaftar, silahkan login disini :</h4>
		<a href="login.php">
				<button style="background-color: #23c25b; color: white; padding: 7px 15px; border-radius: 7px; border: none; font-family: inherit;">
					Login
				</button>
		</a>
	</center>
</body>
</html>