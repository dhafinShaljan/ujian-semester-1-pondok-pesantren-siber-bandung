<?php
session_start();

if ( isset($_SESSION['login']) ) {
	header("Location: index.php");

	exit;
}

require 'fungsi/fungsi_user.php';

if( isset($_POST["login"]) ) {


	$username = $_POST["username"];
	$password = $_POST["password"];
	$_SESSION["username"] = $username;

	$result = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");

	//cek username
	if( mysqli_num_rows($result) === 1 ) {


		//cek password
		$row = mysqli_fetch_assoc($result);
		if (password_verify($password, $row["password"]) ) {
			//set session
			$_SESSION["login"] = true;

			
			header("Location: index.php");
			exit;
		}

	}

	$error = true;

}



?>


<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
	<style type="text/css">

		body{
			background-image: url(assets/img/01.jpg);
			background-size: 1500px;
			color: #edebeb;
			animation: fadeIn 3s;
			font-family: Helvetica;
		}

		h1{
			font-family: "montserratff", helvetica, arial;
		}

		fieldset{
			width: 350px;
		}

		ul li{
			list-style: none;
			margin-top: 5px;
			margin-right: 32.5px;
			padding: 5px;
		}

		input{
			outline: none;
			padding: 5px 10px;
			background-color: #e8e8e8;
			font-family: Helvetica;
			border: solid 3px #e8e8e8;
			border-radius: 5px;
			transition: all 0.5s;
		}

		input:focus{
			background-color: white;
			border: solid 3px #0B8389;
			transition: all 0.5s;
		}


		label{
			display: block;
			font-family: Helvetica;
		}


		button {
			padding: 10px 20px;
			color: white; 
			border-radius: 10px;
			border: none;
			margin-top: 15px;
			font-weight: bold;

		}

		button[type="submit"]{
			background-color: #23c25b;
			border-bottom: solid 2px #036a6f;
		}

		button[type="reset"]{
			background-color: #d12f2f;
			border-bottom: solid 2px #af1f1f;
		}

		button:hover{
			opacity: 0.8;
		}

		#error {
			color: red;
			font-style: italic;
			font-weight: bold;
			font-family: "montserratff", helvetica, arial;
		}

		@keyframes fadeIn{

			from {
				opacity: 0;
			}

			to {
				opacity: 1;
			}
		}
	</style>
</head>
<body>

	<center>
		<h1>Halaman Login</h1>

		<form action="" method="post">
			<fieldset width="500">
			<ul>
				<li>
					<label for="username">Username :</label>
					<input type="text" name="username" id="username" required="">
				</li>
				<li>
					<label for="password">Password :</label>
					<input type="password" name="password" id="password" required="">
				</li>
				<li>
					<button type="submit" name="login">Login</button>
					<button type="reset">Reset</button>
				</li>
				<li>
					<?php if( isset($error) ) : ?>
						<p id="error"> Username atau Password Salah</p>

					<?php endif; ?>
				</li>
			</ul>
			</fieldset>
		</form>

		<h4 style="margin-bottom: 0px;">Anda Belum mendaftar? Silahkan lakukan pendaftaran disini :</h4>
		<a href="register.php">
				<button style="background-color: #23b2c2; color: white; padding: 7px 15px; border-radius: 7px; border: none; font-family: inherit;">
					Daftar
				</button>
		</a>
	</center>

</body>
</html>