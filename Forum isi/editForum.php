<?php  
// session_start();

// if( !isset($_SESSION["login"]) ) {
// 	header("Location: login.php");
// 	exit;
// }

require 'fungsi/fungsi_user.php';
require 'fungsi/config.php';

$row = detailForum($_GET['id']);

?>



<!DOCTYPE html>
<html>
<head>
	<title>Forum Diskusi</title>
	<style type="text/css">
		body 
		{
			background-image:url(assets/img/Forum.jpg);
			background-size:1500px;
			color: #d12f2f;
		}
	</style>
</head>
<body>
	<div>
		<div class="container">
			<div class="edit-forum">
				<form action="" method="post">
					<fieldset>
						<h3>Edit Forum: <?= $row['judul'] ?></h3>
						<div>
							<label for="judul">Judul</label>
							<input type="text" name="judul" id="judul" value="<?= $row['judul'] ?>">
						</div>
						<div>
							<label for="editor">Isi</label>
							<textarea name="isi" id="editor">
								<?= $row['isi'] ?>
							</textarea>
						</div>
						<button type="submit" name="btnedit">
							Edit
						</button>
					</fieldset>
				</form>
			</div>

			<?php if( isset($_POST['btnedit']) ) {
						editForum($_POST, $_GET['id']);
						echo "<meta http-equiv='refresh' content='1;url=index.php'>";
					}
			?>
			
			<a href="index.php">
				<button style="background-color: #d12e2e; color: white; padding: 7px 15px; border-radius: 7px; margin-top: 10px; border: none; font-family: inherit;">
					kembali
				</button>
			</a>
		</div>
	</div>

	<script src="assets/js/ckeditor.js"></script>
	<script src="assets/js/script.js"></script>
</body>
</html>