<?php  
// session_start();

// if( !isset($_SESSION["login"]) ) {
// 	header("Location: login.php");
// 	exit;
// }

require 'fungsi/fungsi_user.php';
require 'fungsi/config.php';



?>



<!DOCTYPE html>
<html>
<head>
	<title>Forum Diskusi</title>
	<style type="text/css">
		body{
			background-image: url(assets/img/06.jpg);
			background-size: 1250px;
			color: white;
			
		}
		a{
			text-decoration: none;
			color: blue;
		}
	</style>
</head>
<body>



	<div class="container">
		<div class="title">
			<h1>Daftar Forum Diskusi</h1>
			<a href="inputForum.php">
				<button>
					Tambah Forum
				</button>
			</a>
			<a href="logout.php">Logout</a>
		</div>

		<table>
			<tr>
				<th>No</th>
				<th>Topik</th>
				<th>Aksi</th>
				<th>Jumlah Komentar</th>
			</tr>
		
		<?php $i = 1; ?>
		<?php $data = tampilForum(); foreach ($data as $row) : ?>
		<tr>
			<td>
				<?= $i ?>
			</td>
			
			<td>
				<h3>
					<a href="detail.php?id=<?= $row['id'] ?>">
							<?= $row['judul'] ?>
					</a>
				</h3>
			</td>
			
			<td>
				<a href="editForum.php?id=<?= $row['id'] ?>" id="edit">
				Edit
			</a>
			|
			<a href="hapusForum.php?id=<?= $row['id'] ?>" onclick="return confirm('Apakah Anda Yakin ingin Menghapus Forum?')" id="hapus">
				Hapus
			</a>
			</td>
			
			<td>
				<?= jumlahKomentar($row['id']); ?> Komentar
			</td>
			
		</tr>
		<?php $i++; ?>
		<?php endforeach ?>
		</table>
	</div>
	

</body>
</html>