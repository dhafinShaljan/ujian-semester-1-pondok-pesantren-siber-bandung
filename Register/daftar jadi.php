<?php 

	require 'fungsi/fungsi_user.php';

	if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	}

	if (isset($_POST["register"]) ) {

		if (registrasi($_POST) > 0) {
			echo "<script>
					alert('user baru berhasil ditambahkan!');
					</script>";
		} else {
			echo mysqli_error($conn);
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Register Berita Kodingan</title>
	<link rel="stylesheet" type="text/css" href="styles.css">

</head>
<body>
<div class="box-form">
<form>
<center>
		<div class="title-form">
			<h1>Berita Kodingan</h1>
			</div>
		<table>
			<tr>
				<td>ID</td>
				<td>:</td>
				<td><input type="text" name="id"></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="text" name="email"></td>
			</tr>
			<tr>
				<td>No.Telefon</td>
				<td>:</td>
				<td><input type="text" name="no.telefon"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" name="tempat lahir"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><input type="Date" name="Tanggal lahir"></td>
			</tr>
			<tr>
				<td>Pendidikan</td>
				<td>:</td>
				<td><select name="Pendaftaran">
				<option value="" ="SD">SD</option>
					<option value="SMP">SMP</option>
					<option value="SMKN">SMKN</option>
					<option value="SMA">SMA</option>
					<option value="Lainnya">Lainnya</option>
				</select></td>
			</tr>
			
			<tr>
				<td>
					<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td><select name="jenis Kelamin">
  					<option value="Laki-Laki">Laki-Laki</option>
  					<option value="Perempuan">Perempuan</option>
 				</select>
				</td>
			</tr>
		<tr>
			<td></td>
			<td></td>
			<td><input type="Submit" name="Simpan"><input type="Reset" name="Batal"></td>
		</tr>
	</table>
</center>		
</form>
</body>
</html>